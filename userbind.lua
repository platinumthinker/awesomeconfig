local awful = require("awful")

dmenu = "dmenu_run -fn 'DejaVu Mono-10:normal' -nb '#002b36' -nf\
    '#657b83' -sb '#002b36' -sf '#fdce59'"

-- {{{ Key bindings
globalkeys = awful.util.table.join(globalkeys,
    keydoc.group("Userbind"),
    awful.key({ modkey,         }, "g",
        function () awful.util.spawn("thunar") end, "Thunar"),
    awful.key({ modkey,         }, "i",
        function () awful.util.spawn("x-www-browser") end, "Internet browser"),
    awful.key({ modkey,         }, "d",
        function () awful.util.spawn(dmenu) end, "List program"),
    awful.key({ modkey, "Shift" }, "w",
        function ()
            awful.util.spawn(awful.util.getdir("config") ..
                "/scripts/winlist.sh " .. command)
        end, "List active windows"),
    awful.key({ modkey,         }, "w",
        function ()
            awful.util.spawn(awful.util.getdir("config") ..
                "/scripts/winctl.sh " .. command)
        end),
    awful.key({ modkey,         }, "p",
        function () awful.util.spawn("xfce4-appfinder") end),
    awful.key({ modkey,         }, "s",
        function () awful.util.spawn("pavucontrol") end, "Sound"),
    awful.key({ modkey,         }, "e",
        function () awful.util.spawn("gvim") end, "Gvim"),
    awful.key({ modkey,         }, "b",
        function () awful.util.spawn("./scripts/lock.sh") end, "Lock workspace"),
    awful.key({ modkey,         }, "F1", keydoc.display)
)
-- }}}
